import { Writable } from 'stream';

import { Article } from './article';
import { Flux } from './flux';

interface Console {
  ajouteFlux(flux: Flux): void;
  afficheTousFlux(): Promise<void>;
}

export class ConsoleStdout implements Console {
  private output: Writable = process.stdout;
  private flux: Flux[] = [];
  private articlesAffiches: Article[] = [];

  public ajouteFlux(flux: Flux): void {
    this.flux.push(flux);
  };

  public async afficheTousFlux(): Promise<void> {
    const articles = (
      await Promise.all(this.flux.map((f) => f.getArticles()))
    ).flat();

    for(const article of articles) {
      if (this.articleDejaAffiche(article)) {
	continue;
      } else {
	this.afficheArticle(article);

	this.articlesAffiches.push(article);
      }
    }
  };

  private afficheArticle(article: Article) {
    this.output.write(`Article n° ${this.articlesAffiches.length + 1}\n`);
    article.affiche(this.output);
    this.output.write('------\n\n');
  };

  private articleDejaAffiche(article: Article) {
    return !!this.articlesAffiches.find((a) => a.egal(article));
  };
}
