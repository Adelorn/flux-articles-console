import { WritableGetSortie } from './writable';

describe('La classe WritableGetSortie', () => {
  const writable = new WritableGetSortie();
  beforeEach(() => writable.reset());

  it('Doit pouvoir renvoyer son entrée', () => {
    const message = 'Récupération des données';

    writable.write(message);

    expect(writable.getSortie()).toBe(message);
  })

  it('Doit pouvoir renvoyer la somme de plusieurs entrées', () => {
    const messageA = 'Récupération des données';
    const messageB = '\nCeci est un second message';

    writable.write(messageA);
    writable.write(messageB);

    expect(writable.getSortie()).toBe(messageA + messageB);
  })
})
