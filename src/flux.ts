import Parser from 'rss-parser';

import { Article, ArticleRSS } from './article'

export interface Flux {
  getArticles(): Promise<Article[]>;
}

export class FluxRSS implements Flux {
  private articles: Promise<Article[]>;
  private rssParser = new Parser();

  constructor(url: string) {
    this.articles = this.getArticlesFromUrl(url);
  }

  public getArticles(): Promise<Article[]> {
    return this.articles;
  }

  private async getArticlesFromUrl(url: string): Promise<Article[]> {
    const feed = await this.rssParser.parseURL(url);
    return feed.items.map((item) => new ArticleRSS(item));
  };
}
