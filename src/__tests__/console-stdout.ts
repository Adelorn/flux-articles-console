import { WritableGetSortie } from "../../test/writable";
import { ConsoleStdout } from "../console";

describe('La console écrivant sur l\'entrée standard', () => {
  const articleA = {
    affiche: jest.fn(),
    egal: jest.fn().mockImplementation((a) => a == doublonArticleA)
  };

  const articleB = {
    affiche: jest.fn(),
    egal: jest.fn().mockReturnValue(false)
  };

  const doublonArticleA = {
    affiche: jest.fn(),
    egal: jest.fn().mockReturnValue(false)
  };

  const mockFluxA = {
    getArticles: jest.fn().mockResolvedValue([
      articleA,
    ])
  };

  const mockFluxB = {
    getArticles: jest.fn().mockResolvedValue([
      articleB,
      doublonArticleA,
    ])
  };

  let consoleStdout: ConsoleStdout;
  beforeEach(() => {
    jest.clearAllMocks();
    consoleStdout = new ConsoleStdout();
    consoleStdout['output'] = new WritableGetSortie();
  });

  it('doit pouvoir afficher les articles d\'un flux', async () => {
    consoleStdout.ajouteFlux(mockFluxA);
    await consoleStdout.afficheTousFlux();

    expect(articleA.affiche.mock.calls.length).toBe(1);
  })

  it('doit pouvoir afficher les articles de deux flux', async () => {
    consoleStdout.ajouteFlux(mockFluxA);
    consoleStdout.ajouteFlux(mockFluxB);
    await consoleStdout.afficheTousFlux();

    expect(articleA.affiche.mock.calls.length).toBe(1);
    expect(articleB.affiche.mock.calls.length).toBe(1);
  })

  it('doit éliminer les articles doublons', async () => {
    consoleStdout.ajouteFlux(mockFluxA);
    consoleStdout.ajouteFlux(mockFluxB);
    await consoleStdout.afficheTousFlux();

    expect(doublonArticleA.affiche.mock.calls.length).toBe(0);
  })
})
