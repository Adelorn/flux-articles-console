import Parser from 'rss-parser';
import { Writable } from 'stream';

export interface Article {
  affiche(sortie: Writable): void;
  egal(autreArticle: Article): boolean;
  getId(): string | undefined;
}

export class ArticleRSS implements Article {
  private item: Parser.Item;

  constructor(item: Parser.Item) {
    this.item = item;
  };

  affiche(sortie: Writable) {
    this.afficheElement(sortie, 'Titre', this.item.title);
    this.afficheElement(sortie, 'Lien', this.item.link);
    this.afficheElement(sortie, 'Description', this.item.content);
  };

  private afficheElement(sortie: Writable, header: string, content: string | undefined) {
    sortie.write(`${header}: `);
    sortie.write(content);
    sortie.write(`\n`);
  }

  egal(autreArticle: Article) {
    if (autreArticle.getId() == undefined || this.getId() == undefined) {
      return false;
    }
    return autreArticle.getId() == this.getId();
  };

  public getId(): string | undefined {
    return this.item.guid || this.item.link || this.item.content;
  }
}
