import { ConsoleStdout } from "./src/console";
import { FluxRSS } from "./src/flux";

const urlsRSS = [
  'https://www.journalduhacker.net/rss',
  'https://sagot.dev/index.xml'
];

const affichage = new ConsoleStdout();

urlsRSS
  .map((u) => new FluxRSS(u))
  .forEach((f) => affichage.ajouteFlux(f));

affichage.afficheTousFlux();
