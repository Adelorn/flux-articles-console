import { Writable } from 'stream';

/**
* @description Classe utilitaire permettant de récupérer ce qui a été
* écrit sous forme d'une chaîne de caractères à l'aide de la méthode
* getSortie
*/
export class WritableGetSortie extends Writable {
  private sortie: string = '';

  public _write(chunk: any, callback?: (error: Error | null | undefined) => void): void;
  public _write(chunk: any, encoding: BufferEncoding, callback?: (error: Error | null | undefined) => void): void;
  public _write(...args: any[]): void {
    this.sortie += args[0].toString();

    if (args.length == 2 && args[1]) {
      args[1]();
    }
    if (args.length == 3 && args[2]) {
      args[2]();
    }
  }

  public getSortie() {
    return this.sortie;
  }

  public reset() {
    this.sortie = '';
  }
}
