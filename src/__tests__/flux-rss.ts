import { FluxRSS } from '../flux';
import Parser from 'rss-parser';

jest.genMockFromModule('rss-parser');
jest.mock('rss-parser');

const mockParser = {
  parseURL: jest.fn().mockReturnValue({ items: [] })
};

(Parser as jest.Mock).mockImplementation(() => mockParser);

describe('La classe FluxRSS', () => {
  it('doit parser le flux passé en argument', () => {
    const urlExemple = 'https://example.com/index.xml';
    new FluxRSS(urlExemple);

    expect(mockParser.parseURL.mock.calls[0][0]).toBe(urlExemple);
  })

  it('doit pouvoir retourner liste d\'articles vide', async () => {
    const urlExemple = 'https://example.com/index.xml';
    const flux = new FluxRSS(urlExemple);

    let articles = await flux.getArticles();

    expect(articles).toEqual([]);
  })
})
