import Parser from 'rss-parser';

import { ArticleRSS } from '../article';
import { WritableGetSortie } from '../../test/writable';

const itemExemple: Parser.Item = {
  title: 'Article de test',
  link: 'https://sagot.dev/articles',
  content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis blandit
nunc consectetur dui convallis tempus eget vel lacus. Fusce semper
tortor id nunc consequat consectetur. Praesent vitae feugiat
mi. Vivamus venenatis ex erat, ac maximus ipsum faucibus et. Vivamus
rhoncus, lectus ultrices scelerisque fermentum, nibh odio mattis
libero, id porta leo leo non nisi. Cras accumsan eu felis a
laoreet. Duis non pellentesque felis, in sagittis magna. Mauris
venenatis, urna vitae mollis porta, lacus velit molestie dolor, in
imperdiet nisi nisi eu quam. Praesent felis arcu, pulvinar vitae
commodo sed, laoreet ut velit. Sed a magna neque. Praesent iaculis at
enim in tristique.`
};

const article = new ArticleRSS(itemExemple);

describe('Le retour de la méthode affiche', () => {
  const sortie = new WritableGetSortie();
  beforeEach(() => sortie.reset());

  it('doit contenir le titre de l\'article', () => {
    article.affiche(sortie);

    expect(sortie.getSortie()).toMatch(itemExemple.title as string);
  })

  it('doit contenir le lien vers l\'article', () => {
    article.affiche(sortie);

    expect(sortie.getSortie()).toMatch(itemExemple.link as string);
  })

  it('doit contenir les premiers mots de l\'article', () => {
    article.affiche(sortie);

    expect(sortie.getSortie()).toMatch('Lorem ipsum dolor sit amet');
  })

  it('doit se terminer par un retour à la ligne', () => {
    article.affiche(sortie);

    expect(sortie.getSortie()).toMatch(/\n$/);
  })
})

describe('Un ArticleRSS', () => {
  it('doit être égal à lui-même', () => {
    expect(article.egal(article)).toBe(true);
  })
})
